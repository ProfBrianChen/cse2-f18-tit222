//Tiffany Truong 

import java.util.Scanner; //import scanner 

public class hw07{ 


	//sampleText method 
	public static String sampleText(){ 
		Scanner scan = new Scanner(System.in); //asks user for input 
		System.out.print("Enter a sample text: "); 
		String input= scan.nextLine();//assigns input to input 
 
		System.out.println("You entered: " + input );//prints user's input 
		return input;//returns input 

	}

	//printMenu(); 
	public static char printMenu(){ //prints all the options!! 
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters"); 
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");

		Scanner scan = new Scanner(System.in); 
		System.out.print("Enter a character: "); //asks user for what command they want 
		char input= scan.next().toLowerCase().charAt(0);//makes sure the input in lower case
 
		while(true){ //loop that asks the user over and over again for a command, just an outline of what it really is going to be 
			switch(input){ 
				case 'c': 
					//dostuff
					return input;
				case 'w':
					//dostuff
					return input;
				case 'f': 
					//dostuff
					return input;
				case 'r':
					//dostuff
					return input;
				case 's': 
					//dostuff
					return input;
				case 'q':
					//dostuff
					return input;
				default:
				System.out.print("Enter a valid character: "); //asks user for valid char
				input = scan.next().toLowerCase().charAt(0);//sets new input and tests it again in the loop
			}//end of switch 
		}

	}

	//getNumOfNonWSCharacters()method 
	public static int getNumOfNonWSCharacters(String text){
		int charNum = 0; 
		int total= 0; 
		int blankNum = 0; 
		for(int place = 0; place<text.length(); place++ ){//loop that goes through the input length and counts the blanks
			if(text.charAt(place) == ' '){
				blankNum++;
			} 
			total= (text.length() - blankNum);//adds all blank spaces and subtracts it from the text length, giving us the the num of non white spaces 
			
		}
		return total; //returns the total of non white space char 
	}


	//Number of words
	public static int getNumOfWords(String text){ 
		text += " ";
		int charNum = 0; 
		int total= 0; 
		int words = 0; 
		int place = 0;
		do{
			if(place<text.length()){//goes through each place in the length of the text
				if(text.charAt(place+1) == ' '){//if it is empty after the last char in a word, it will add 1 to the num of words 
				words++;
				}
			}	
			place++;
		 }while(place<text.length()-1);

		 total= (words); 
	return total;//returns total of words 
	}
	
	//findText() method
	public static int findText(String a,String b){
		int length = a.length();//length of sentence 
		int wordlength = b.length();//word length 
		
		String temp= " ";
		int count=0; 
		for(int i =0; i<=length - wordlength; i++){//loops until the whole length has been checked 
			temp = a.substring(i, i + wordlength);//part of the input that is the word that is being found 
			if(temp.equals(b)){
				count++;
			}

		}
		return count;

		} 
	//replaceExclamation method
	public static String replaceExclamation(String a){
		String sentence = a;
		String newSentence = sentence.replace('!','.');//replaces ! with . 
		while(true){
			newSentence = sentence.replace('!','.');
			if(!sentence.equals(newSentence)){//it will keep replacing until the new sentence has all its ! replaced 
			sentence = newSentence;
			}
			else{
				break;//breaks when it is done 
			}
		}

		return newSentence;//returns the string of the new sentence 
	}

	//shortenSpace method
	public static String shortenSpace(String a){
		String sentence = a;
		String newSentence = sentence.replace("  "," " );//replaces double spaces with single ones 
		while(true){
			newSentence = sentence.replace("  "," ");//loops until no double spaces exist 
			if(!sentence.equals(newSentence)){
			sentence = newSentence;
			}
			else{
				break;
			}
		}

		return newSentence;//returns new sentence 

	}




	public static void main(String[] args){

		String input = sampleText();//sets the user input to value 


		char command = ' ';		
		int output= 0;		

		do{//loops till the user wnats to quit 
			//each command will call the method and the method returns the output, that is then printed 
		command = printMenu();
		switch(command){ 
				case 'c': 
					output= getNumOfNonWSCharacters(input);
					System.out.println("Number of non-white space characters: " + output);
					break;
				case 'w':
					output= getNumOfWords(input);
					System.out.println("Number of words: " + output);
					break;
				case 'f': 
					Scanner scan = new Scanner(System.in); 
					System.out.print("Enter a word or letter to find: "); 
					String findthis= scan.nextLine();
					output= findText(input, findthis); 
					System.out.println("Input instances: " + output);
					break;
				case 'r':
					String out= " ";
					out= replaceExclamation(input); 
					System.out.println("New output: " + out);
					break;
				case 's': 
					String out1= " ";
					out1= shortenSpace(input); 
					System.out.println("New output: " + out1);
					break;
					
		}
		}while(command != 'q');




	}//end of main
}//end of class 



