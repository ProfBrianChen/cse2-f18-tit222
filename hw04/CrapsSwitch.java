//Tiffany Truong 
//9.23.18 
//This program will be able to 
//1.) ask user if they want to cast dice randomly/input two dice values
//2.) If they choose randomly cast dice, two random numbers will be generated
//3.) Scanner will ask twice for two integers between 1-6
//4.) Determine slang terminology of the outcome of the roll 
//5.) Print out slang terminology 
//6.) Exit
//7.) ALL USING SWITCH STATEMENTS. 

//Import Scanners into java
import java.util.Scanner; 

//State class and main method
public class CrapsSwitch{ 
public static void main (String[] args){
  
  //1.) Choosing Scanner or Random number
  Scanner input = new Scanner(System.in);
  //Declaring dice variables 
  int dice1= 0;
  int dice2= 0;
  //input for user when choosing random or scanner
  System.out.print("Enter 1 if you would like to use scanner. Enter 2 if you would like to use random numbers: ");
  double number = input.nextDouble();
  
  //SCANNER: If user types 1, scanner will allow user to type in numbers.
  if (number==1){
 //Prompt user to input first dice value
  System.out.print("Enter a number between 1 and 6 for dice 1: ");
  dice1= input.nextInt(); 
  System.out.print("Enter a number between 1 and 6 for dice 2: "); 
  dice2= input.nextInt(); 
  }//closing scanner
  
  //RANDOM: If user types 2, random numbers will be generated. 
  else if (number==2){
   dice1= (int)(((Math.random()) * 6) +1);
   dice2= (int)(((Math.random()) * 6) +1); 
   System.out.println("Dice 1 value is " + dice1 + ".");
   System.out.println("Dice 2 value is " + dice2 + ".");
  }//closing scanner


switch (dice1){
    //The value of dice1 will determine what case will be run through by java. 
    //After being assigned to a case for dice1, the program will run through the values of dice2 until it matches the input value.
    //The output is then determined after the input values are matched with the output name that correspond with the values. 
    //This is repeated for each "block."
  case 1: dice1= 1;
    switch (dice2){
           case 1: dice2=1;
           System.out.println("Snake Eyes");
        break;
           case 2: dice2=2;
           System.out.println("Ace Deuce");
        break;
           case 3: dice2=3;
           System.out.println("Easy Four");
        break;
           case 4: dice2=4;
           System.out.println("Fever Five");
        break;
           case 5: dice2=5; 
           System.out.println("Easy Six");
        break;
           case 6: dice2=6;
           System.out.println("Seven Out");
        break;
    } break;
  case 2: dice1= 2;
    switch (dice2){
          case 1: dice2=1;
          System.out.println("Ace Deuce");
        break;
          case 2: dice2=2;
          System.out.println("Hard Four");
        break;
          case 3: dice2=3;
          System.out.println("Fever Five");
        break;
          case 4: dice2=4;
          System.out.println("Easy Six");
        break;
          case 5: dice2=5; 
          System.out.println("Seven Out");
        break;
          case 6: dice2=6;
          System.out.println("Easy Eight");
        break;
    } break;
  case 3: dice1= 3;
    switch (dice2){
          case 1: dice2=1;
          System.out.println("Easy Four");
        break;
          case 2: dice2=2;
          System.out.println("Fever Five");
        break;
          case 3: dice2=3;
          System.out.println("Hard Six");
        break;
          case 4: dice2=4;
          System.out.println("Seven Out");
        break;
          case 5: dice2=5; 
          System.out.println("Easy Eight");
        break;
          case 6: dice2=6;
          System.out.println("Nine");
        break;
    } break;

  case 4: dice1= 4;
    switch (dice2){
          case 1: dice2=1;
          System.out.println("Fever Five");
        break;
          case 2: dice2=2;
          System.out.println("Easy Six");
        break;
          case 3: dice2=3;
          System.out.println("Seven Out");
        break;
          case 4: dice2=4;
          System.out.println("Hard Eight");
        break;
          case 5: dice2=5; 
          System.out.println("Nine");
        break;
          case 6: dice2=6;
          System.out.println("Easy Ten");
        break;
    } break;
    case 5: dice1= 5;
    switch (dice2){
          case 1: dice2=1;
          System.out.println("Easy Six");
        break;
          case 2: dice2=2;
          System.out.println("Seven Out");
        break;
          case 3: dice2=3;
          System.out.println("Easy Eight");
        break;
          case 4: dice2=4;
          System.out.println("Nine");
        break;
          case 5: dice2=5; 
          System.out.println("Hard Ten");
        break;
          case 6: dice2=6;
          System.out.println("Yo-leven");
        break;
    } break;
     case 6: dice1= 6;
    switch (dice2){
          case 1: dice2=1;
          System.out.println("Seven Out");
        break;
          case 2: dice2=2;
          System.out.println("Easy Eight");
        break;
          case 3: dice2=3;
          System.out.println("Nine");
        break;
          case 4: dice2=4;
          System.out.println("Easy Ten");
        break;
          case 5: dice2=5; 
          System.out.println("Yo-leven");
        break;
          case 6: dice2=6;
          System.out.println("Boxcars");
        break;
    } break; 

}
}//end of main method 
}//end of class

        
        

        
                 
    