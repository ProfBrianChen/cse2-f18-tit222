//Tiffany Truong 
//9.23.18 
//This program will be able to 
//1.) ask user if they want to cast dice randomly/input two dice values
//2.) If they choose randomly cast dice, two random numbers will be generated
//3.) Scanner will ask twice for two integers between 1-6
//4.) Determine slang terminology of the outcome of the roll 
//5.) Print out slang terminology 
//6.) Exit
//7.) ALL USING IF STATEMENTS

//Import Scanners into java
import java.util.Scanner; 

//State class and main method
public class CrapsIf{ 
public static void main (String[] args){
  
  //1.) Choosing Scanner or Random number
  Scanner input = new Scanner(System.in);
  //Declaring dice variables 
  int dice1= 0;
  int dice2= 0;
  //input for user when choosing random or scanner
  System.out.print("Enter 1 if you would like to use scanner. Enter 2 if you would like to use random numbers: ");
  double number = input.nextDouble();
  
  //SCANNER: If user types 1, scanner will allow user to type in numbers.
  if (number==1){
 //Prompt user to input first dice value
  System.out.print("Enter a number between 1 and 6 for dice 1: ");
  dice1= input.nextInt(); 
  System.out.print("Enter a number between 1 and 6 for dice 2: "); 
  dice2= input.nextInt(); 
  }//closing scanner
  
  //RANDOM: If user types 2, random numbers will be generated. 
  else if (number==2){
   dice1= (int)(((Math.random()) * 6) +1);
   dice2= (int)(((Math.random()) * 6) +1); 
   System.out.println("Dice 1 value is " + dice1 + ".");
   System.out.println("Dice 2 value is " + dice2 + ".");
  }//closing scanner
  
  //IF STATEMENTS
  if (dice1==1){ //If Dice1=1, then the options for dice2 will be run through here.
    //The output name will depend on the value of Dice2 and what its name is when dice1=1. 
    //This repeats when the value of dice1 changes. 
    if (dice2==1){
      System.out.println("Snake Eyes");}
    else if (dice2 ==2){
      System.out.println("Ace Deuce");}
    else if (dice2 ==3){
      System.out.println("Easy Four");}
    else if (dice2==4){
       System.out.println("Fever Five");}
    else if (dice2==5){
       System.out.println("Easy Six");}
    else if (dice2 ==6){
      System.out.println("Seven Out");}
  }
  
  
if (dice1==2){
    if (dice2==1){
      System.out.println("Ace Deuce");}
    else if (dice2 ==2){
      System.out.println("Hard Four");}
    else if (dice2 ==3){
      System.out.println("Fever Five");}
    else if (dice2==4){
       System.out.println("Easy Six");}
    else if (dice2==5){
       System.out.println("Seven Out");}
    else if (dice2 ==6){
      System.out.println("Easy Eight");}
  }
if (dice1==3){
    if (dice2==1){
      System.out.println("Easy Four");}
    else if (dice2 ==2){
      System.out.println("Fever Five");}
    else if (dice2 ==3){
      System.out.println("Hard Six");}
    else if (dice2==4){
       System.out.println("Seven Out");}
    else if (dice2==5){
       System.out.println("Easy Eight");}
    else if (dice2 ==6){
      System.out.println("Nine");}
  }
  if (dice1==4){
    if (dice2==1){
      System.out.println("Fever Five");}
    else if (dice2 ==2){
      System.out.println("Easy Six");}
    else if (dice2 ==3){
      System.out.println("Seven Out");}
    else if (dice2==4){
       System.out.println("Hard Eight");}
    else if (dice2==5){
       System.out.println("Nine");}
    else if (dice2 ==6){
      System.out.println("Easy Ten");}
  }
  if (dice1==5){
    if (dice2==1){
      System.out.println("Easy Six");}
    else if (dice2 ==2){
      System.out.println("Seven Out");}
    else if (dice2 ==3){
      System.out.println("Easy Eight");}
    else if (dice2==4){
       System.out.println("Nine");}
    else if (dice2==5){
       System.out.println("Hard Ten");}
    else if (dice2 ==6){
      System.out.println("Yo-leven");}
  }
  if (dice1==6){
    if (dice2==1){
      System.out.println("Seven Out");}
    else if (dice2 ==2){
      System.out.println("Easy Eight");}
    else if (dice2 ==3){
      System.out.println("Nine");}
    else if (dice2==4){
       System.out.println("Easy Ten");}
    else if (dice2==5){
       System.out.println("Yo-leven");}
    else if (dice2 ==6){
      System.out.println("Boxcars");}
  }

}//end of main method 
}//end of class 
  
  

 

 
  
  
  
   

  


