//Tiffany Truong 


import java.util.Scanner; //import scanner 


public class hw08{ 


	//method shuffle (list) 
	public static void shuffle(String[] list){ 
		for ( int i = 0; i <52; i++){
			//generate an index j randomly with 0 <= j <= i
			int j = (int)((Math.random()*(50)+1)); //int j is a random number from 0-52

			//Swap list[i] with list[j]
			String temp = list[0]; // temp is the string value of the "first value" in the index
			list[0] = list[j]; //the first value is going to be equal to the random integer now 
			list[j] = temp; //this switches the value of list[0] to list[j] so that that cards "switch" instead of just being replaced

		}//end for 

	}//end of method shuffle 

//method getHand(list,index, numCards)
	public static String[] getHand(String []list, int index, int numcards){//will return an array of strings 
		String[]value = new String[numcards]; //new array called value, will be an array equal to the number of cards drawn for the hand 
		for(int i=0; i<numcards; i++){//will run until the number of cards requested for the hand is exhausted 
			value[i] = list[index-i]; // the value of i (which ever card number) will be equal to the TOP CARD (last index number, index is 51)

		}//end of for

		return value; // returns the array of the hand of cards! 
	}//end of getHand 


//method printArray(list)
	public static void printArray(String [] list){// will print the cards 

		for( int i=0; i < list.length; i++){// goes through the whole list length 

			System.out.print( list[i] + " ");// pritns the array (deck of cards, with a space in between)

		}//end of for 

	}//end of printArray





public static void main(String[] args){

	Scanner scan = new Scanner(System.in); 
	//suits club, heart, spade or diamond 
	String[] suitNames={"C","H","S","D"}; //array for the suit names    
	String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //array for rank names 
	String[] cards = new String[52]; //array for the deck of cards 
	String[] hand = new String[5]; //array for the hand of cards 
	int numCards = 5; // number of cards drawn for the hand 
	int again = 1; // conditional for the for loop
	int index = 51; // index in the array of the deck of cards. It is 51 because the deck starts at "0" and goes to 51, which is 52 space values 
	

	for (int i=0; i<52; i++){ //sets a card value to each space value in the array of the deck of cards 
	  cards[i]=rankNames[i%13]+suitNames[i/13]; //rank name, suitname 
	  //System.out.println(cards[i]+" "); 
	} 
	System.out.println(); //prints a line 
	printArray(cards); //prints the original deck of cards 

	System.out.println();//prints a line 
	shuffle(cards); //calls method shuffle, using the array of the deck of cards
	printArray(cards); //prints shuffled deck 

	//printArray(cards); 
	while(again == 1){ //again is set to 1 ^^^^^^ 
   hand = getHand(cards,index,numCards); //hand is an array of 5 
   System.out.println(""); //prints line
   printArray(hand); //prints the hand 
   index = index - numCards; //takes away for the index by the number of cards 
   if(index < numCards){
   	shuffle(cards);
   	index = 51; 
   }
   System.out.println("Enter a 1 if you want another hand drawn"); //asks if the user wants another hand drawn 
   again = scan.nextInt(); //if again is 1 the loop will run again. if not, the loop will break. 
}  
   





}//end of class
}//end of main 