//Tiffany Truong 
//9.9.18
//Purpose: This program will be able to calculate 
// a.) Total cost of each kind of item (i.e. total cost of pants, etc)
// b.) Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
// c.) Total cost of purchases (before tax) 
// d.) Total sales tax
// e.) Total paid for this transaction, including sales tax

public class Arithmetic {
  //main method required for every Java program 
  public static void main (String[] args) { 
  //Quantity of each item
  int numPants=3; //number of pair of pants 
  int numShirts=2; //number of sweatshirts 
  int numBelts=1; //number of belts
  //Price of each item
  double pantsPrice=34.98; //pants price 
  double shirtsPrice=24.99; //shirt price 
  double beltPrice=33.99; //belt price 
  double paSalesTax=0.06; //pa Sales Tax 
    
    // a.) Total costs of each item 
    double totalPantsPrice= pantsPrice*numPants; 
    double totalShirtsPrice= shirtsPrice*numShirts;
    double totalBeltPrice= beltPrice*numBelts;
    System.out.println("The total pants price is:" + "$" + totalPantsPrice + ".");
    System.out.println("The total shirt price is:" + "$" + totalShirtsPrice + ".");
    System.out.println("The total belt price is:" + "$" + totalBeltPrice + ".");
    
    // b.) Sales Tax for each item
    double taxPants= pantsPrice*paSalesTax;
    double taxShirts= shirtsPrice*paSalesTax;
    double taxBelts= beltPrice*paSalesTax;
    // Explicit Casting 
    double taxPants2= (int) (taxPants*numPants*100)/(100.0);
    double taxShirts2= (int) (taxShirts*numShirts*100)/(100.0);
    double taxBelts2= (int) (taxBelts*numBelts*100)/(100.0);
    System.out.println("The sales tax for pants is:" + "$" + taxPants2 + ".");
    System.out.println("The sales tax for shirts is:" + "$" + taxShirts2 + ".");
    System.out.println("The sales tax for belts is:" + "$" + taxBelts2 + ".");
   
    // c.) Total cost of purchases (before tax) 
    double totalPriceNoTax= (totalPantsPrice + totalShirtsPrice + totalBeltPrice);
    // Explicit Casting 
    double totalPriceNoTax2= (int) (totalPriceNoTax*100)/(100.0);
    System.out.println("The total cost of purchases is: " + "$" + totalPriceNoTax2 + "." );
 
    // d.) Total sales tax
    double totalSalesTax= ((taxPants*3) + (taxShirts*2) + (taxBelts*1));
    // Explicit Casting 
    double totalSalesTax2= (int) (totalSalesTax*100)/(100.0);
    System.out.println("The total sales tax is:" + "$" + totalSalesTax2 + ".");
    
    // e.) Total paid for this transaction, including sales tax
    double totalTransaction= (totalPriceNoTax*paSalesTax) + (totalPriceNoTax);
    // Explicit Casting 
    double totalTransaction2= (int) (totalTransaction*100)/(100.0);
    System.out.println("The total transaction is:" + "$" + totalTransaction2 + ".");
 
    /* INITIAL MISTAKES
    // b.) Sales Tax for each item
    System.out.println("numPants"*"pantsPrice"*"paSalesTax");
    System.out.println("numShirts"*"shirtPrice"*"paSalesTax");
    System.out.println("numBelts"*"beltPrice"*"paSalesTax");
   
    // c.) Total cost of purchases (before tax)
    System.out.println("numPants"*"pantsPrice");
    System.out.println("numShirts"*"shirtPrice");
    System.out.println("numBelts"*"beltPrice"); 
    
    // d.) Total sales tax 
    System.out.println(("numPants"*"pantsPrice"*"paSalesTax")
                       +("numShirts"*"shirtPrice"*"paSalesTax")+("numBelts"*"beltPrice"*"paSalesTax"));
    
    // e.) Total paid for transaction, including sales tax
    System.out.println((("numPants"*"pantsPrice") + ("numPants"*"pantsPrice"*"paSalesTax"))+
                      (("numShirts"*"shirtPrice") + ("numShirts"*"shirtPrice"*"paSalesTax"))
                      (("numBelts"*"beltPrice") + ("numBelts"*"beltPrice"*"paSalesTax")));
    
   */ 
  
  } //end of main method
} //end of class