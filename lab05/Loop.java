//Tiffany Truong
//10.4.2018
//This program will ask a user for 
//a.) Course number 
//b.) Department name 
//c.) Number of times they meet per week 
//d.) Time class starts 
//e.) Instructor name 
//f.) Number of students

import java.util.Scanner; //import scanner 

public class Loop{ 
public static void main(String[] args){ 
Scanner scan= new Scanner(System.in); //declare scanner 
  
//a.) COURSE NUMBER 
  System.out.print("Enter course number: "); 
  int coursNum=0;
  
  boolean correctinput= true;
  while(correctinput){
    boolean correct = scan.hasNextInt();
  if (correct){
    coursNum = scan.nextInt();
    correctinput = false;
  }
  else{ 
    scan.next();
    System.out.println("Error: You must enter an integer for course number."); 
    }//end else statement
  }//end loop 
  
//b.) DEPARTMENT NAME
   System.out.print("Enter department name: "); 
  String depName=" ";
  
  correctinput= true;
  while(correctinput){
    boolean correct = scan.hasNext();
  if (correct){
    depName = scan.next();
    correctinput = false;
  }
  else{ 
    scan.next();
    System.out.println("Error: You must enter a string for department name."); 
    }//end else statement
  }//end loop 
  
 //c.) NUMBER OF MEETINGS PER WEEK
  System.out.print("Enter number of meetings per week: "); 
  int numWeek=0;
  
  correctinput= true;
  while(correctinput){
    boolean correct = scan.hasNextInt();
  if (correct){
    numWeek = scan.nextInt();
    correctinput = false;
  }
  else{ 
    scan.next();
    System.out.println("Error: You must enter an integer for number of meetings per week."); 
    }//end else statement
  }//end loop 
 
//d.) TIME CLASS STARTS 
   System.out.print("Enter starting time of class: "); 
  String startTime=" ";
  
  correctinput= true;
  while(correctinput){
    boolean correct = scan.hasNext();
  if (correct){
    startTime = scan.next();
    correctinput = false;
  }
  else{ 
    scan.next();
    System.out.println("Error: You must enter a string for starting time of class."); 
    }//end else statement
  }//end loop 
  
//e.) INSTRUCTOR NAME 
  System.out.print("Enter instructor name: "); 
  String teachName=" ";
  
  correctinput= true;
  while(correctinput){
    boolean correct = scan.hasNext();
  if (correct){
    teachName= scan.next();
    correctinput = false;
  }
  else{ 
    scan.next();
    System.out.println("Error: You must enter a string for instructor name."); 
    }//end else statement
  }//end loop 
  
 //f.) Number of students
  System.out.print("Enter number of students: "); 
  int numStudents=0;
  
  correctinput= true;
  while(correctinput){
    boolean correct = scan.hasNext();
  if (correct){
    numStudents = scan.next();
    correctinput = false;
  }
  else{ 
    scan.next();
    System.out.println("Error: You must enter an integer for number of students."); 
    }//end else statement
  }//end loop 
 
  

}//end main method
}//end class 
  

 
