//Tiffany Truong
//9.20.18
//a.) By using if statements, switch statements, and
//the modulus operator, a random number (input) will be choosen and 
//a random card will be the output.

public class CardGenerator{
  public static void main (String args[]){
    //GOING TO CHOOSE RANDOM NUMBER THAT WILL HAVE A CARD VALUE.
    int number=(int)((Math.random()*52 +1)); 
    //The random number chosen will first be assigned to the interval of the suite
    //and then to its case number.
  
    //DIAMONDS: Cards 1-13. 
    if (number<=13 && number>=1){
      String card= "diamonds";
      switch (number){ 
        case 1: 
          System.out.println("You picked the ace of diamonds.");
            break;
        case 2:
           System.out.println("You picked the 2 of diamonds.");
             break;
        case 3:
          System.out.println("You picked the 3 of diamonds.");
            break;
        case 4: 
          System.out.println("You picked the 4 of diamonds.");
            break;
        case 5:
          System.out.println("You picked the 5 of diamonds.");
            break;
        case 6:
          System.out.println("You picked the 6 of diamonds.");
            break;
        case 7:
          System.out.println("You picked the 7 of diamonds.");
            break;
        case 8:
          System.out.println("You picked the 8 of diamonds.");
            break;
        case 9:
          System.out.println("You picked the 9 of diamonds.");
            break;
        case 10:
          System.out.println("You picked the 10 of diamonds.");
            break;
        case 11:
          System.out.println("You picked the Jack of diamonds.");
            break;
        case 12:
          System.out.println("You picked the Queen of diamonds.");
            break;
        case 13:
          System.out.println("You picked the King of diamonds.");
            break;
      } 
    }
    //CLUBS: Cards 14-26.
    if (number<=26 && number>=14){
      String card= "clubs";
      switch (number){ 
        case 14: 
          System.out.println("You picked the ace of clubs.");
            break;
        case 15:
           System.out.println("You picked the 2 of clubs.");
             break;
        case 16:
          System.out.println("You picked the 3 of clubs.");
            break;
        case 17: 
          System.out.println("You picked the 4 of clubs.");
            break;
        case 18:
          System.out.println("You picked the 5 of clubs.");
            break;
        case 19:
          System.out.println("You picked the 6 of clubs.");
            break;
        case 20:
          System.out.println("You picked the 7 of clubs.");
            break;
        case 21:
          System.out.println("You picked the 8 of clubs.");
            break;
        case 22:
          System.out.println("You picked the 9 of clubs.");
            break;
        case 23:
          System.out.println("You picked the 10 of clubs.");
            break;
        case 24:
          System.out.println("You picked the Jack of clubs.");
            break;
        case 25:
          System.out.println("You picked the Queen of clubs.");
            break;
        case 26:
          System.out.println("You picked the King of clubs.");
            break;
    }
  }
    //HEARTS: Cards 27-39.
    if (number<=39 && number>=27){
      String card= "hearts";
      switch (number){ 
      case 27: 
          System.out.println("You picked the ace of hearts.");
            break;
        case 28:
           System.out.println("You picked the 2 of hearts.");
             break;
        case 29:
          System.out.println("You picked the 3 of hearts.");
            break;
        case 30: 
          System.out.println("You picked the 4 of hearts.");
            break;
        case 31:
          System.out.println("You picked the 5 of hearts.");
            break;
        case 32:
          System.out.println("You picked the 6 of hearts.");
            break;
        case 33:
          System.out.println("You picked the 7 of hearts.");
            break;
        case 34:
          System.out.println("You picked the 8 of hearts.");
            break;
        case 35:
          System.out.println("You picked the 9 of hearts.");
            break;
        case 36:
          System.out.println("You picked the 10 of hearts.");
            break;
        case 37:
          System.out.println("You picked the Jack of hearts.");
            break;
        case 38:
          System.out.println("You picked the Queen of hearts.");
            break;
        case 39:
          System.out.println("You picked the King of hearts.");
            break;
    }
    } 
    
    //SPADES: Cards 40-52.
     if (number<=52 && number>=40){
       String card= "spades";
       switch (number){
      case 40: 
          System.out.println("You picked the ace of spades.");
            break;
        case 41:
           System.out.println("You picked the 2 of spades.");
             break;
        case 42:
          System.out.println("You picked the 3 of spades.");
            break;
        case 43: 
          System.out.println("You picked the 4 of spades.");
            break;
        case 44:
          System.out.println("You picked the 5 of spades.");
            break;
        case 45:
          System.out.println("You picked the 6 of spades.");
            break;
        case 46:
          System.out.println("You picked the 7 of spades.");
            break;
        case 47:
          System.out.println("You picked the 8 of spades.");
            break;
        case 48:
          System.out.println("You picked the 9 of spades.");
            break;
        case 49:
          System.out.println("You picked the 10 of spades.");
            break;
        case 50:
          System.out.println("You picked the Jack of spades.");
            break;
        case 51:
          System.out.println("You picked the Queen of spades.");
            break;
        case 52:
          System.out.println("You picked the King of spades.");
            break;
       
       
     }  
     }

    
  }//end of main method
}//end of class