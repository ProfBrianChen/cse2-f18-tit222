//Tiffany Truong 
//11.26.18 

import java.util.Scanner; //import scanner 
import java.util.Random;//import random num generator 


public class CSE2Linear{ //create class 

	//Scrambling method 
		public static int[] scrambled(int[] grades){
			Random random = new Random();// create random 
			int counter= 0; //will be used to store the current variable 

			for(int k = 0; k<grades.length; k++){// loop to mix up the numbers 
				int rand = random.nextInt(15);// the random number being generated 0-15
				counter = grades[rand];// sets counter equal to a random element in array grades 
				grades[rand] = grades[k];// sets the random array element equal to 0, the first element in the array 
				grades[k] = counter; //sets the first element equal to whatever random element in the array was 
			}
		return grades;//returns the new scrambled array 
	} //end of scrambled method 


	//print method 
		public static void printArray(int[] grades){//method to print the array 
				for (int j = 0; j < grades.length; j++){//runs as many times as the length of the array
					System.out.print(grades[j] + " ");//prints out the array. Note: j is just the element place, not the value of j 
			}//end of print loop 
			System.out.println();// print line 
		}//end of print method 


	//linear search method 
	public static void linear(int[] grades, int x){
		boolean helper= false;//helper 
		for(int i=0; i<grades.length; i++){//searches array length 
			if(grades[i]== x){//if the value at grades i is equal to the pos
				System.out.print( x + " is found at position " + i + ".");//if element is found it will return the place value of x in the array 
				helper = true;//helper will be true 
			}//end of if 
			
		}//end of for 
		if(helper == false ){//when helper is falso it will print an error 
		System.out.print( x + " was not found.");//if element not found 
		}
		
	}//end of linear search method 

	//binary search method 
	public static void binary(int[] grades, int x){
		int low= 0;//low of array
		int high= grades.length-1;//high of array
		int counter= 0; //counter
		int mid= 0;//mid of array
		while(low<= high){//if the low is less than or equal to the high
			 mid = ((low + high)/2);//the middle will be the average of the two 
			if (x == grades[mid]){// then if the target is equal to the middle, the target is found
				System.out.print( x + " was found at postion: " + mid + " with " + counter + " iterations. "); 
				break;
			}//end of mid
			if (x < grades[mid]){//if the target is less than the middle 
				high = mid - 1;//high will be set to the middle 
				counter++;//counter will be added (counts the iteration)
			}//end of mid 
			else{
				low = mid + 1;//low will be equal to middle if x neither less than mid, or if low is less than high 
				counter++;//iteration 
			}//end of else 
		}
		if (x != grades[mid]){//if target is not found after the loop, it is not in the array 
			System.out.print("Input was not found in the list with 4 iterations.");
		}//end of if 

	}


	public static void main(String []args){//main method 

		Scanner scan = new Scanner(System.in);//creating scanner 
		System.out.print("Enter 15 ascending ints for final grades in CSE2: "); //prompts user to enter input
		int grades[]= new int[15]; //array for the list of grades 
		int before= -1; // int must not be less than the one before it (ascending order). Set to -1 to satisfy first condition
		int current= 0;// current int 
		double current2= 0;
		

		//1.) Ascending order
		//2.) In range 
		//3.) an integer 
		for(int i=0; i< grades.length; i++){
			//CHECK FOR ASCENDING ORDER 
				boolean trueInt = scan.hasNextInt(); //is it an int , yes or no
				if ( trueInt ){//if yes 
					current = scan.nextInt();//current will equal the input 
				}
				else{//if no
					System.out.println("Input is not an integer. Enter a new list of valid integers: " );//input will not be an int 
					//i--;//make sure loop runs just as many times 
					scan.next();
					for(int t=0; t< grades.length; t++){//clears array if given wrong input
						grades[t]= 0;
					}
					i=0;//resets i to 0 
					current=0;	//resets the current value 

				}
				if(current>= before){//if the current number is greater than the one before it 
					if(current>=0 && current<=100){
						grades[i]= current;//then grades[i] will be set equal to that int
						before = current;//current is now equal to before 
					}//end of if 
				}//end of if checking ascending order
				
				else{
					System.out.println("Int is not greater than or equal to the last int. Enter a new list of valid integers: ");//print 
					//i--;//make sure loop runs just as many times as it would with a correct int
					for(int r=0; r< grades.length; r++){//clears array if given wrong input
						grades[r]= 0;
					}
					i=0;//resets i
					current = 0;//resets current number 
				}//end of else checking for ascending order 
		


			//CHECK IF IN RANGE 
				if(current>=0 && current<=100){//condition 
					grades[i]= current;//then grades[i] will be set equal to that int
					before = current;//current is now equal to before 
				}//end of if 
				else{
					System.out.println("Input is not in range. Enter a new list of valid integers: ");
					//i--;//make sure loop runs just as many times 
					for(int z=0; z< grades.length; z++){//clears array 
						grades[z]= 0;
					}	
					i=0;
					current = 0;			
				}
			
		}//end of for 

		printArray(grades);//prints grade 
		//printing the scrambled grades 
		System.out.print("Enter an integer to search for: ");//asks user for target 
		int y = scan.nextInt(); //y is set equal to target 
		binary(grades, y);//calls binary 
		


		grades = scrambled(grades);//scrambles grades
		System.out.println();//prints line
		System.out.print("Scrambled: ");
		printArray(grades);//prints scrambled grades
		System.out.print("Enter an integer to search for: ");//asks for target 
		int x = scan.nextInt();//sets x to target 
		linear(grades, x);//calls method 
		
	}//end of main 

}//end of class 