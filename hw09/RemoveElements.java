//Tiffany Truong 
//11.27.18

import java.util.Scanner;
public class RemoveElements{


    //random input method 
   public static int[] randomInput(){

    int array[] = new int[10];
    for(int i= 0; i<10; i++){
    array[i] = (int)(Math.random()*9); 
    }
    return array;
  }//end of random input method

  //method delete 
  public static int[] delete(int []list, int pos){
    int j=0;//counter that will go through the index of the smaller array 
    int []smallerArray= new int[list.length-1];//create copy for smaller array 
      for(int i=0; i<list.length; i++){//goes through the length of the orignal array 
        if( i != pos){//if the index in list does not equal to the postion, then it will be added to the new Array
           smallerArray[j] = list[i];//smaller array will be equal to list i! 
          j++;//index will go up for smaller array
          }//end of if  
       }//end of for 
       return smallerArray;
  }//end of method delete 

    //method remove
    public static int[] remove(int[] list,int target){
      int[]array2 = new int[list.length];//creates copy 
      int k= 0;//counter 
      for(int i=0; i<list.length;i++){//goes through array lienght 
        if(list[i] != target){//if target is equal to the value in the position of the list then
          array2[k] = list[i];//the value will be copied to the new array
          k++;//counter for k 
        }//end of if 
      }//end of for 
    int[] finalArray2= new int[k];//makes final new array with right amount of storage 
    for(int j=0; j<k; j++){//loop
      finalArray2[j]= array2[j];//sets all value of array2 equal to the final array
    }//end of for 
    return finalArray2;//returns final array
    }//end of remove method 


    public static void main(String [] arg){
	
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
	  String answer="";
	     do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput(); 
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  
  
  }

} //end of main 
 
