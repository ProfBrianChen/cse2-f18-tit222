///Tiffany Truong 
//10.7.18 
//This program will take an integer value indicating the number of times the your program should generate hands. The program will print the number of loops as well as probability associated with each of the four hands. 

import java.util.Scanner;//import scanner 

public class CardLoops{ 
public static void main (String[] args){


Scanner scan = new Scanner(System.in);//declare scanner 

//1.) USER INPUT: USER MUST ENTER IN INTEGER OF NUMBER OF TIMES HANDS WILL BE GENERATED 
  System.out.print("Enter an integer: ");
  
  int numHands=0; //USER INPUT VALUE: NUMBER OF HANDS THAT WILL BE GENERATED
  int four=0; //NUMBER OF FOUR OF A KIND 
  int three=0; //NUMBER OF THREE OF A KIND 
  int two=0; //NUMBER OF TWO OF A KIND 
  int one=0; //NUMBER OF ONE OF A KIND 
  
  boolean correctinput= true;//Creating a generic conditional for the while loop, just to check if input is an integer
  while(correctinput){
    boolean correct = scan.hasNextInt(); //if input is an integer, it will be assigned 'correct'
  if (correct){
    numHands = scan.nextInt(); //numHands will be assigned the value of the user input 
    correctinput = false; //breaks the loop
  }// end of if statement 
  else{ 
    scan.next(); //clears the scanner input 
    System.out.println("Error: You must enter an integer for course number."); 
    }//end of else statement
  }//END OF LOOP1: CHECKING IF INPUT IS INTEGER


//2.) GENERATING RANDOM CARDS
  int current=0;
  int card1=(int)((Math.random()*52 +1)); 
  int card2=(int)((Math.random()*52 +1)); 
  int card3=(int)((Math.random()*52 +1));
  int card4=(int)((Math.random()*52 +1));
  int card5=(int)((Math.random()*52 +1)); 

//DEBUGGING
  //System.out.print("card1: " + card1 );
  //System.out.print("card2: " + card2 );
  //System.out.print("card3: " + card3 );
  //System.out.print("card4: " + card4 );
  //System.out.print("card5: " + card5 );

//3.) MAKE SURE CARDS DO NOT REPEAT
  while (current<= numHands){
  card1=(int)((Math.random()*52 +1)); 
  do{ 
    card2=(int)((Math.random()*52 +1)); 
  }while(card2==card1);//this will check to make sure card 1 != card 2 REPEAT FOR ALL CARDS
  do{
    card3=(int)((Math.random()*52 +1));
  }while(card3==card2||card3==card1);
  do{
     card4=(int)((Math.random()*52 +1));
  }while(card4==card3||card4==card2||card4==card1); 
  do{
    card5=(int)((Math.random()*52 +1)); 
  }while(card5==card4||card5==card3||card5==card2||card5==card1);
  current++;
 

//DEBUGGING
  //System.out.print("Card 1: " + card1 + "."); 
  //System.out.print("Card 2: " + card2 + "."); 
  //System.out.print("Card 3: " + card3 + "."); 
  //System.out.print("Card 4: " + card4 + ".");
  //System.out.print("Card 5: " + card5 + ".");

//4.) GENERATING THE HANDS. THE SOLTUION TO EACH MODULUS PROBLEM WILL BE EQUAL TO A CERTAIN FACE VALUE. 
  int face1= card1%13; 
  int face2= card2%13;
  int face3= card3%13;
  int face4= card4%13;
  int face5= card5%13;

  /*System.out.print("Face 1: " + face1);
  System.out.print("Face 2: " + face2);
  System.out.print("Face 3: " + face3);
  System.out.print("Face 4: " + face4);
  System.out.print("Face 5: " + face5);*/
 
  /*System.out.print("Face 1: " + face1);
  System.out.print("Face 2: " + face2);
  System.out.print("Face 3: " + face3);
  System.out.print("Face 4: " + face4);
  System.out.print("Face 5: " + face5);*/

//5.)LIST ALL THE PROBALITIES OF HAVING A FOUR/THREE/TWO/ONE OF A KIND.\

//5a. FOUR OF A KIND POSSIBILITIES
  

    if ((face1 == face2 && face2 == face3 && face3 == face4) || 
         (face2 == face3 && face3 == face4 && face4 == face5) ||
         (face3 == face4 && face4 == face5 && face5 == face1) ||
         (face4 == face5 && face5 == face1 && face1 == face2) ||
         (face5 == face1 && face1 == face2 && face2 == face3)){
      four++;
    }

//5b. THREE OF A KIND POSSIBILITIES
    
    if((face1 == face2 && face2 == face3) ||
          (face2 == face3 && face3 == face4) ||
          (face3== face4 && face4 == face5)  ||
          (face4 == face5 && face5 == face1) ||
          (face5 == face1 && face1== face2)  ||
          (face1 == face3 && face3 == face3) ||
          (face1 == face2 && face2 == face4) ||
          (face2 == face3 && face3 == face5) ||
          (face2 == face4 && face4 == face5) ||
          (face1 == face3 && face3== face5)) {
      three++;
    }

//5c. TWO OF A KIND POSSIBILITIES
        if((face1 == face2 && face3 == face4)||
          (face1 == face2 && face3 == face5)||
          (face1 == face2 && face4 == face5)||
          (face1 == face3 && face2 == face4)||
          (face1 == face3 && face2 == face5)||
          (face1 == face3 && face4 == face5)||
          (face1 == face4 && face2 == face3)||
          (face1 == face4 && face2 == face5)||
          (face1 == face4 && face3 == face5)||
          (face1 == face5 && face2 == face3)||
          (face1 == face5 && face2 == face4)||
          (face1 == face5 && face3 == face4)||
          (face2 == face3 && face4 == face5)||
          (face3 == face4 && face2 == face5)) {
          two++;
        }
        

//5d. ONE OF A KIND POSSIBILITIES
        if((face1 == face2 && face3 != face4 && face4 != face5 && face3 != face5)||
          (face1 == face3 && face2 != face4 && face4 != face5 && face2 != face5)||
          (face1 == face4 && face2 != face3 && face3 != face5 && face2 != face5)||
          (face1 == face5 && face2 != face3 && face3 != face4 && face2 != face4)||
          (face2 == face3 && face1 != face4 && face4 != face5 && face1 != face5)||
          (face2 == face4 && face1 != face3 && face3 != face5 && face1 != face5)||
          (face2 == face5 && face1 != face3 && face3 != face4 && face1 != face4)||
          (face3 == face4 && face1 != face2 && face2 != face5 && face1 != face5)||
          (face3 == face5 && face1 != face2 && face2 != face4 && face1 != face4)||
          (face4 == face5 && face1 != face2 && face2 != face3 && face1 != face3)){
          one++;
        }
        current++; 

      }//END OF LOOP2: CHECKING CARDS TO NOT REPEAT 

    
      
  //calculating probability
    double fourProb = (double)four/numHands;
    double threeProb = (double)three/numHands;
    double twoProb = (double)two/numHands;
    double oneProb = (double)one/numHands;
    

     System.out.println("The number of hands drawn is " + numHands);
     System.out.printf("The probability of four of a kind is %.3f\n", fourProb);
     System.out.printf("The probability of three of a kind is %.3f\n", threeProb);
     System.out.printf("The probability of two pairs is %.3f\n", twoProb);
     System.out.printf("The probability of one pair is %.3f\n", oneProb);
       


   
}//end of main method  
}//end of class 
