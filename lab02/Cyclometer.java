//Tiffany Truong 
//9.6.18 CSE02 
//Purpose: This program will be able to 
//a.) print the number of minutes for each trip
//b.) print the number of counts for each trip
//c.) print the distance of each trip in miles
//d.) print the distance for the two trips combined
//
public class Cyclometer {
    // main method required for every Java program
  public static void main (String[] args) {
  int secTrip1=480; //declares that it is seconds per trip, sets value at 480 
  int secTrip2=3220; //declares that it is seconds per trip2, sets value at 3220 
  int countsTrip1=1561; //declares that it is counts per trip1, sets value at 1561
  int countsTrip2=9037; //declares that it is counts per trip2, sets value at 9037

  double wheelDiameter=27.0; //declares wheel diameter and its value, necessary for distance calculation 
  double PI=3.14159; // declares the value of PI 
  int feetPerMile=5280; // declares value, helps calculate distance 
  int inchesPerFoot=12; // declares value, and translates feet to inches
  int secondsPerMinute=60; // declares the value and helps calculate time
  double distanceTrip1, distanceTrip2, totalDistance; //

  System.out.println("Trip 1 took "+ (secTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
  System.out.println("Trip 2 took "+ (secTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
    //run calculations; store the values.
    //
  distanceTrip1=countsTrip1*wheelDiameter*PI;
    //Above gives distance in inches
    //(for each count, a rotation of the wheel travels 
    //the diameter in inches times PI)
  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles 
  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; 
  totalDistance=distanceTrip1+distanceTrip2;
    
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
  } //end of main method 
} //end of class 

