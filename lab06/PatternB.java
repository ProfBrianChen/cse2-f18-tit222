//Tiffany Truong 
//10.11.18 

import java.util.Scanner;//import scanner 

public class PatternB{
	public static void main(String[] args){
//ASKS USER FOR INTEGER 
Scanner key = new Scanner(System.in);
System.out.print("Enter an integer between 1 and 10: "); 
int rows=0; //declares variable but not value

//CHECK IF INPUT IS BETWEEN 1-10 INCLUSIVE
boolean correct= true;
while(correct){

	boolean yesInt= key.hasNextInt();//CHECK IF INPUT IS INTEGER 
	if (yesInt){ 
	if(rows>=1 || rows<=10){//check if input is between 1 and 10
	rows = key.nextInt();
	break;//breaks loop
	}//end of if statement yesint 
	else{ //if input is not an integer 
	key.next();//clears scanner class
				System.out.print("You must enter an integer between 1 and 10: ");
			}//end of else statment "not an integer"
			}//end of if statement 1-10
			}//END OF LOOP CHECKING IF INTEGER FOR ROWS IS BETWEEN 1-10

int num;//declares a variable so it can be used in loop 
for(int i=rows; i>0; i--){

	
	for(int j=1; j<=i; j++){//inner for loop
		System.out.print(j + " "); //prints out numbers
		
	}

	System.out.println();//prints a line for next row

}//end of outter loop i=0



	}//end of main method
}//end of class