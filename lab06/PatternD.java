//Tiffany Truong 
//10.11.18

import java.util.Scanner; 

public class PatternD{ 
public static void main(String[] args){

//ASKS USER FOR INTEGER 
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter an integer between 1-10: "); 
		int rows = 0;//declares variable but not value

		//CHECK IF INPUT IS BETWEEN 1-10 INCLUSIVE
		boolean correct= true;
		while(correct){
			boolean yesInt = scan.hasNextInt();//CHECK IF INPUT IS INTEGER 
				if(yesInt){
				
				if(rows>=1 || rows<=10){//check if input is between 1 and 10
				rows = scan.nextInt();
				break;//breaks loop
				}//end of if statement yesint 
				else{ //if input is not an integer 
				scan.next();//clears scanner class
				System.out.print("You must enter an integer between 1 and 10: ");
			}//end of else statment "not an integer"
			}//end of if statement 1-10
			}//END OF LOOP CHECKING IF INTEGER FOR ROWS IS BETWEEN 1-10

//outter loop to hanndle number of rows 
for(int i=rows; i>0; i--){

//inner loop to handle number of columns
			//values will change according to outter loop		
for(int j=i, k=1; k<=i; j--, k++){
	System.out.print(j + " ");//prints the numbers 
}
System.out.println(" ");//prints line for new row

}//end of outter loop 


}//end of main 
}//end of class