//Tiffany Truong 
//10.11.18 

import java.util.Scanner;//import scanner 

public class PatternA{
	public static void main(String[] args){
		
		//ASKS USER FOR INTEGER 
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter an integer between 1-10: "); 
		int rows = 0;//declares variable but not value

		//CHECK IF INPUT IS BETWEEN 1-10 INCLUSIVE
		boolean correct= true;
		while(correct){
			boolean yesInt = scan.hasNextInt();//CHECK IF INPUT IS INTEGER 
				if(yesInt){
				    if(rows>=1 && rows<=10){//check if input is between 1 and 10
				    rows = scan.nextInt();
				    break;//breaks loop
				}//end of if statement yesint 
				else{ //if input is not an integer 
				scan.next();//clears scanner class
				System.out.print("You must enter an integer between 1 and 10: ");
			}//end of else statment "not an integer"
			}//end of if statement 1-10
			}//END OF LOOP CHECKING IF INTEGER FOR ROWS IS BETWEEN 1-10

		
		//outter loop to hanndle number of rows 
		for(int i=1; i<=rows; i++){

			//inner loop to handle number of columns
			//values will change according to outter loop
			int num=1;
			for(int j=1; j<=i; j++){
				System.out.print(j + " ");//prints the number s
				num++;//add one to num being printed 
			}

			System.out.println();//prints a line for new row 

			}//end of outter loop 


	

		





	}//END OF MAIN METHOD 
}//END OF CLASS 

