//Tiffany Truong 
//9.16.18
//This program will be able to calculate the number of acres
//of land affected by hurrican precipitation and 
//how many inches of rain were dropped on average. 
//The quantity of rain will be converted to into cubic miles.

import java.util.Scanner; 

public class Convert {
  //mainmethod required for every Java program 
  public static void main (String[] args) {
  //Construct scanner 
    Scanner myScanner = new Scanner (System.in); 
     
  //Declare vairables
    double affectedAcres;
    double rainfall; 
    double totalMiles;
  //input for the number acres of land 
    System.out.print("Enter the number of acres of land in acres: ");
    affectedAcres = myScanner.nextDouble();
  //input for amount of rainfall 
    System.out.print("Enter the amount of rainfall in the affected area in inches: ");
    rainfall = myScanner.nextDouble(); 
  //Calculations
    
    //acres = 43560 square feet
    //acres to feet, then to inches
    affectedAcres= (affectedAcres * 43560) * Math.pow(12.0,2.0);
    //calculates cubic miles
    totalMiles= ((affectedAcres * rainfall)/Math.pow(12.0,3.0))/Math.pow(5280,3);
    System.out.println("The total number of of cubic miles is " + totalMiles + ".");
   
  } //end of main method
  
} //end of class 
