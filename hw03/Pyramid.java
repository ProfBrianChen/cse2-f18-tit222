//Tiffany Truong 
//9.16.18
//This program will be able to calculate the volume inside a square pyramid.

import java.util.Scanner; 

public class Pyramid {
  //main method for every java program 
  public static void main (String[] args) {
    //Construct Scanner 
    Scanner myScanner = new Scanner (System.in); 
    
    //Declaring variables
    double length; 
    double height;
    double volume;
    //input for length 
    System.out.print("Enter the length: ");
    length = myScanner.nextDouble();
    //input for height 
    System.out.print("Enter the height: ");
    height = myScanner.nextDouble();
    
    //Calculations 
    
    volume = ((Math.pow(length,2.0)) * height * (1.0/3) );
    System.out.println ("The volume of the pyramid is " + volume + ".");
    
    
  } //end of main method
} //end of class

