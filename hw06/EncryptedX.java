//Tiffany Truong 
//10.23.18 

//PROGRAM OBJECTIVE: 
//This program will print an encrypted X. 
//The encrypted X will be a square. 
//The Square's size will depend on the user's input 

import java.util.Scanner;//import scanner 

public class EncryptedX{
public static void main(String[] args){

Scanner scan= new Scanner(System.in);//declare and construct scanner 
System.out.print("Enter an integer from 0-100: "); //ask user for input 
int input= 0;// declare input, but the assigned varibale isn't permanent 


//check if input is an integer 
boolean correct= true; 
while(correct){
boolean intTrue= scan.hasNextInt();//checks if input is an integer  
if(intTrue){
	if(input>=0||input<=100){//checks if input is between 1-100 
		input= scan.nextInt(); //sets input equal to the user's input 
		break;
	}//end if(input>=1||input<=100)
}//end if(intTrue)
else{ 
scan.next();
System.out.print("You must enter an integer betweeen 0-100: ");
}//end else 
} //end of while(correct)


int rows;//declare rows 
int columns; //declare columns 

for(rows=0; rows<input; rows++){//number of times outter loop will run for the rows

	for(columns=0; columns<input; columns++){//number of times loop will run for the columns 

		if(rows==columns || input-rows-1==columns){//instances where there will need to be a space for any given input 
			System.out.print(" ");//print space 
		}
		else{ //if not a condition where a space is needed, a "*" will be printed 
			System.out.print("*");
		}


	}//end of first loop
	System.out.println();//prints new line for each row 
}//end of second loop 

 





}//end of main method 
}//end of class